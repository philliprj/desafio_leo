<?php session_start();?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LEO</title>

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--link href="../assets/css/fa-icons.css" rel="stylesheet"-->


    <script src="../assets/js/jquery.min.js"></script>

    <script>
      $(document).ready(function(){
        
        <?php
          if(!isset($_SESSION['modal-info'])){ 
            $_SESSION['modal-info'] = 1;
            echo "$('#modal-info').modal();";
          }
        ?>

      });
    </script>
    
  </head>
  <div id="modal-info" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <img src="../assets/img/modal-programmer.png" alt="">
      <div class="modal-header">
        <button type="button" class="close style-close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>    
    </div>

  </div>
</div>

  <body>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.html"><img src="../assets/img/logo-header.png"/></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item pesquisa">
            <form class="form-group input-group">
              <div id="imaginary_container"> 
                  <div class="input-group stylish-input-group">
                      <input type="text" class="form-control"  placeholder="Search" >
                      <span class="input-group-addon">
                          <button type="submit">
                                <img src="../assets/img/icon-search.png" alt="">
                          </button>  
                      </span>
                  </div>
              </div>
            </form>
            </li>
            <hr class="style-hr">
            <li class="nav-item">
              <div class="style-student-account">
                John Doe
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
