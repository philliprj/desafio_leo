
    <!-- Footer -->
    <footer class="py-5 bg-dark">
    <div class="container">
      <div class="row footer-style">
        <div class="col-xs-6">
          <a class="navbar-brand" href="index.html">
            <img src="../assets/img/logo-footer.png"/>
          </a>
        </div>
        <div class="col-xs-6">
          <div class="row contact-social">
            <div class="col-md-6">
              <h4 class="title-social-icons">//CONTATO<h4>
              <ul class="list contact">
                <li class="list-item">
                  (21) 98765-2837
                </li>
                <li class="list-item">
                  contato@leolearning.com
                </li>             
              </ul>
            </div>
            <div class="col-md-6 style-social-icons">
              <h4 class="title-social-icons">//REDES SOCIAIS<h4>
              <div class="container">
                <ul class="list-inline social-buttons">
                  <li class="list-inline-item">
                    <a href="#">
                      <i class="fa fa-twitter"></i>
                    </a>
                    </li>
                    <li class="list-inline-item">
                      <a href="#">
                    <i class="fa fa-facebook"></i>
                    </a>
                    </li>
                    <li class="list-inline-item">
                      <a href="#">
                    <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>      
  </footer>
  
      <div class="footer-bottom">
          <div class="container">
              <p class="pull-left"> Copyright © Footer 2014. All right reserved. </p>
          </div>
      </div>
      <script src="../assets/js/popper.min.js"></script>
      <script src="../assets/js/bootstrap.min.js"></script>
  
    </body>
  
  </html>
  