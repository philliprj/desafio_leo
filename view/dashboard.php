<?php require 'header.php';
$idUser = $_GET['aluno'];
?>
<script>
$(document).ready(function(){

    var dashboard = "", url = "../frontController.php?action=listStudentsCourses&param=<?=$idUser?>";

    $.ajax({
        url: url,
        cache: false,
        dataType: "json",
        error: function() {
            $("h2").html("Algo de estranho aconteceu. Tente novamente mais tarde!");
        },
        success: function(data) {
            if(data[0].erro){
                $("h2").html(data[0].erro);
            }
            else{
                for(var i = 0; i<data.length; i++){
                    dashboard += "<div class='col-lg-3 col-sm-6 portfolio-item'>";
                    dashboard += "<div class='card h-100 style-border'>";
                    dashboard += "<a href='#'><img class='card-img-top' src='../assets/img/courses/" + data[i].img + "' alt=''></a>";
                    dashboard += "<div class='card-body'>";
                    dashboard += "<h4 class='card-title'>";
                    dashboard += "<a href='#'>" + data[i].name + "</a>";
                    dashboard += "</h4>";
                    dashboard += "<p class='card-text'>" + data[i].description + "</p>";
                    dashboard += "<a href='#' class='btn btn-primary style-button'>Continuar</a>";
                    dashboard += "</div>";
                    dashboard += "</div>";
                    dashboard +="</div>";
                }
                dashboard += "<div id='add-course' class='col-lg-3 col-sm-6 portfolio-item'>";
                dashboard += "<div class='card h-100 style-border'>";
                dashboard += "<a href='#'><img class='add-course' src='../assets/img/courses/add-course.png' alt=''></a>";
                dashboard += "</div>";
                dashboard += "</div>";
                $("#dashboard-courses").html(dashboard);
            }
        }
    });

    var slider = "", url = "../frontController.php?action=slideshow";

    $.ajax({
        url: url,
        cache: false,
        dataType: "json",
        error: function() {
            $("h2").html("Algo de estranho aconteceu. Tente novamente mais tarde!");
        },
        success: function(data) {
            if(data[0].erro){
                $("h2").html(data[0].erro);
            }
            else{
                var active = "active";

                //d-none
                for(var i = 0; i<data.length; i++){
                    if(i!=0){active=""}
                    slider += "<div class='carousel-item "+active+"' style='background-image: url(../assets/img/slider/" + data[i].img + ")'>";
                      slider += "<div class='carousel-caption d-md-block'>";
                        slider += "<div class='card style-card'>";
                        slider += "<div class='card-body style-card-body'>";
                        slider += "<h3>" + data[i].title + "</h3>";
                        slider += "<p>" + data[i].description + "</p>";
                        slider += "<a href='artigos/"+ data[i].url +"' class='btn btn-primary button-slider'>Leia mais</a>";
                        slider += "</div>";
                        slider += "</div>";
                      slider += "</div>";
                    slider += "</div>";


                }

                $(".carousel-inner").html(slider);

            }
        }

    });

    $(document).on( "click", "#add-course", function() {

        var courseList = "", url = "../frontController.php?action=courseList";

        $.ajax({
            url: url,
            cache: false,
            dataType: "json",
            error: function() {
                $("h2").html("Algo de estranho aconteceu. Tente novamente mais tarde!");
            },
            success: function(data) {
                if(data[0].erro){
                    $("h2").html(data[0].erro);
                }
                else{   
                    for(var i = 0; i<data.length; i++){
                        courseList += "<option>"+ data[i].name +" - "+ data[i].description +"</option>";
                    }
                    $("#add-course").html(courseList);
                }
            }
        });

        $('#modal-add-course').modal();
    });
});
</script>

<div id="modal-add-course" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h3>Escolha o seu novo curso!<h3>
        </div>
        <div class="modal-body">
        <div class="form-group">
            <select class="form-control" id="add-course"></select>
            </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        </div>    
        </div>
    </div>
</div>


<header>
    <h2></h2>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">

    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</header>
<div class="container">
  <h3 class="my-4">MEUS CURSOS</h3>
  <hr>
  <div class="row" id="dashboard-courses"></div>
</div>

<?php require 'footer.php'; ?>
