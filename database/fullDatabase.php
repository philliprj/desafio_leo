<?php
$db_file = "leo.db";
$db = new SQLite3($db_file);

//Tabela de cursos
$query = "CREATE TABLE IF NOT EXISTS course (id INTEGER PRIMARY KEY AUTOINCREMENT , name STRING, description STRING, img STRING)";
$test = $db->exec($query);

$query = "INSERT INTO course (name, description, img) VALUES ('Java', 'Curso de Java Iniciante', 'java.png')";
$db->exec($query);
$query = "INSERT INTO course (name, description, img) VALUES ('Java', 'Curso de Java Intermediario', 'java.png')";
$db->exec($query);
$query = "INSERT INTO course (name, description, img) VALUES ('Java', 'Curso de Java Avançado', 'java.png')";
$db->exec($query);

$query = "INSERT INTO course (name, description, img) VALUES ('PHP', 'Curso de PHP Iniciante', 'php.png')";
$db->exec($query);
$query = "INSERT INTO course (name, description, img) VALUES ('PHP', 'Curso de PHP Intermediario', 'php.png')";
$db->exec($query);
$query = "INSERT INTO course (name, description, img) VALUES ('Java', 'Curso de PHP Avançado', 'java.png')";
$db->exec($query);


//Tabela de cursos por estudantes
$query = "CREATE TABLE IF NOT EXISTS student_course (id_student INTEGER, id_course INTEGER)";
$db->exec($query);

$query = "INSERT INTO student_course (id_student, id_course) VALUES (1, 1)";
$db->exec($query);

$query = "INSERT INTO student_course (id_student, id_course) VALUES (1, 2)";
$db->exec($query);

$query = "INSERT INTO student_course (id_student, id_course) VALUES (1, 4)";
$db->exec($query);

//Tabela de estudantes
$query = "CREATE TABLE IF NOT EXISTS student (id INTEGER PRIMARY KEY AUTOINCREMENT , name STRING)";
$db->exec($query);

$query = "INSERT INTO student (name) VALUES ('John Doe')";
$db->exec($query);

//Tabela do slider
$query = "CREATE TABLE IF NOT EXISTS slider (position INTEGER, title STRING, url STRING, description STRING, img STRING)";
$db->exec($query);

$query = "INSERT INTO slider (position, title, url, description, img) VALUES (2, 'DevOps', 'devops.php', 'O que é devops?', '1-min.png')";
$db->exec($query);

$query = "INSERT INTO slider (position, title, url, description, img) VALUES (1, 'AWS', 'aws.php', 'Tutorial para subir seu primeiro projeto para produção.', '2-min.png')";
$db->exec($query);

$query = "INSERT INTO slider (position, title, url, description, img) VALUES (3, 'SEO','seo.php', 'Dicas de como deixar o seu site nos top 10 na busca do google.', '3-min.png')";
$db->exec($query);

?>
