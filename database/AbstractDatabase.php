<?php

abstract class AbstractDatabase
{
    private $host;
    private $dbname;
    private $pass;
    private $user;


    
    public function setHost($host){
        $this->host = $host;
    }

    public function getHost(){
        return $this->host;
    }

    public function setDbname($dbname){
        $this->dbname = $dbname;
    }

    public function getDbname(){
        return $this->dbname;
    }

    public function setPass($pass){
        $this->pass = $pass;
    }

    public function getPass(){
        return $this->pass;
    }

    public function setUser($user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }

    abstract public function connect();


}
