<?php
namespace model;
require_once "autoload.php"; 

use database\Database;


class Course
{
	private $id;
	private $name;
	private $description;
	private $img;
  	private $db;

	function __construct(){
		$this->db = new Database();
	}

	public function getImg(){
		return $this->img;
	}

	public function setImg($img){
		$this->img = $img;
	}

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function toArray()
	{
		return [
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'img' => $this->getImg()
		];
	}

	function __toString(){
		return $this->name;
	}

  function list(){
    $query = "select * from course";
	$result = $this->db->query($query);
	return $this->db->resultset();
  }

  function insert(Course $course){
	$query = "insert into course (name, description, img) values (:name,:description,:img)";
	$this->db->query($query);  
	$this->db->bind(':name',$course->getName());
	$this->db->bind(':description',$course->getDescription());
	$this->db->bind(':img',$course->getImg());
	$this->db->execute();
  }

  function update($id,Course $course){
	$name = $course->getName();
	$description = $course->getDescription();
	$img = $course->getImg();

	$c = new Course();
	$c->search($id);
	if(empty($name) || !isset($name)){
		$name = $c->getName();
	}

	if(empty($description) || !isset($description)){
		$description = $c->getDescription();
	}

	if(empty($img) || !isset($img)){
		$img = $c->getImg();
	}

  	$query = "update course set name = :name ,description = :description, img = :img where id = :id";
	$this->db->query($query);
	$this->db->bind(':name',$name);
	$this->db->bind(':description',$description);
	$this->db->bind(':img',$img);
	$this->db->bind(':id',$id);
	$this->db->execute();
  }



  function search($id){

  	$query = "select * from course where id = :id";
  	$this->db->query($query);
	$this->db->bind(':id',$id);  
	$finded_course = $this->db->resultset();

  	$name = $finded_course[0]['name'];
  	$description = $finded_course[0]['description'];
    $img = $finded_course[0]['img'];
	//var_dump($finded_course);
    $this->setName($name);
    $this->setDescription($description);
    $this->setImg($img);
	$this->setId($finded_course[0]['id']);

  }

  function delete($id){
  	$query = "delete from course where id = :id";
	$this->db->query($query);
	$this->db->bind(':id',$id);  
	$this->db->execute();
  }

}
