<?php
namespace model;
require_once "autoload.php"; 

use database\Database;

class Slider
{
    private $position;
    private $title;
    private $img;
    private $url;
    private $description;
    private $db;

    function __construct(){
        $this->db = new Database();
    }

    public function getPosition(){
        return $this->position;
    }

    public function setPosition($position){
        $this->position = $position;
    }

    public function getTitle() {
    	return $this->title;
    }

    public function setTitle($title){
    	$this->title = $title;
    }

    public function getImg(){
    	return $this->img;
    }

    public function setImg($img){
    	$this->img = $img;
    }

    public function getUrl(){
        return $this->url;
    }

    public function setUrl($url){
        $this->url = $url;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    public function toArray()
    {
        return [
            'position' => $this->getPosition(),
            'title' => $this->getTitle(),
            'img' => $this->getImg(),
            'url' => $this->getUrl(),
            'description' => $this->getDescription()

        ];
    }

    function getSlider(){
        
        $arraySlider = array();
        $query = "select * from slider order by position";
        $this->db->query($query);
        $result = $this->db->resultset();
        return $result;
        
    }
}
