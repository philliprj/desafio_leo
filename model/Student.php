<?php
namespace model;
require_once "autoload.php"; 

use database\Database;

class Student
{
    private $id;
    private $name;
    private $courses = array();
    private $db;

    function __construct(){
        $this->db = new Database();
    }

    public function getCourses(){
        return $this->courses;
    }

    public function setCourses($courses){
        $this->courses = $courses;
    }

    public function getId() {
    	return $this->id;
    }

    public function setId($id){
    	$this->id = $id;
    }

    public function getName(){
    	return $this->name;
    }

    public function setName($name){
    	$this->name = $name;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'courses' => $this->getCourses()
        ];
    }

    function getStudentWithCourses($id){
        
        $this->db->query('select * from student where :id = id');
        $this->db->bind(':id',$id);
        $finded_student = $this->db->resultset();
        
        $name = $finded_student[0]['name'];

        $query = "select id_course from student_course where id_student = :id";
        $result = $this->db->query($query);
        $this->db->bind(':id',$id);
        $courses_finded = $this->db->resultset();
        $courses = array();
        foreach($courses_finded as $c){
            array_push($courses,$c['id_course']);
        }
        
        //var_dump($courses);
        
        $this->setName($name);
        $this->setCourses($courses);
        $this->setId($finded_student[0]['id']);
        

        /*
        $query = "select * from student where id = {$id}";
        $result = $this->db->query($query);
        $finded_student = $result->fetchArray();

        $name = $finded_student['name'];

        $query = "select id_course from student_course where id_student = {$id}";
        $result = $this->db->query($query);

        $courses = array();
        while ($courses_finded = $result->fetchArray()){
          array_push($courses,$courses_finded['id_course']);
        }

        $this->setName($name);
        $this->setCourses($courses);
        $this->setId($finded_student['id']);
        */
    }
}
