<?php
ini_set('display_errors',true);
require_once "autoload.php"; 

use controller\Controller;

$action = $_GET['action'];
if(!isset($_GET['param'])){
    $param = null;
} else {
    $param = $_GET['param'];
}


$controller = new Controller($action,$param);
