<?php
namespace controller;
require_once "autoload.php"; 


use model\Course;

class CourseController
{
    public function list(){
      $course = new Course();
      echo json_encode($course->list());
  }
}
