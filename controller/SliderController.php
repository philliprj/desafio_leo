<?php
namespace controller;
require_once "autoload.php"; 

use model\Slider;

class SliderController
{
    public function slider(){
      $slider = new Slider();
      echo json_encode($slider->getSlider());
  }
}
