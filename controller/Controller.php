<?php
namespace controller;
require_once "autoload.php";

class Controller 
{
    function __construct($action,$param){
        switch ($action) {
            case 'listStudentsCourses':
                $this->studentsCourses($param);
                break;
            case 'slideshow':
                $this->slider();
                break;
            case 'courseList':
                $this->coursesList();
                break;
        }
    }

    public function slider() {
        //retorna json com slider
        $slider = new SliderController();
        $slider->slider();
    }

    public function studentsCourses($id){
        //retorna json com os cursos dos alunos
        $studentController = new StudentController();
        $studentController->listStudentCoursesJson($id);
    }

    public function coursesList(){
        $course = new CourseController();
        $course->list();
    }
}