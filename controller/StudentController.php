<?php
namespace controller;
require_once "autoload.php"; 

use model\Student;
use model\Course;


class StudentController
{
    public function listStudentCoursesJson($idStudent){
      $arrayCourses = array();
      $student = new Student();

      $student->getStudentWithCourses($idStudent);
      $listCourses = $student->toArray();

      foreach($listCourses['courses'] as $c){
        $course = new Course();
        $course->search($c);
        array_push($arrayCourses,$course->toArray());
      }
      echo json_encode($arrayCourses);
  }
}
