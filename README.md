# Desafio LEO

## Só executar o index.php

### Informações

#### Database

O projeto utiliza Sqlite3. O banco leo.db já está criado na pasta database. 

Na pasta database existe o arquivo fullDatabase.php com todas as queries utilizadas para esse desafio. Caso queira criar um novo banco, só executar este arquivo.

#### Front end

Foi utilizado bootstrap, jquery e customização no arquivo style.css. Contidos na pasta assets.

#### Back end

PDO para conexão com o banco

Padrão MVC e Autoload das clases.

Namespaces model, controller e database. 






